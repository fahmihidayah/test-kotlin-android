package com.widsons.testkotlin

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter
import com.hannesdorfmann.mosby3.mvp.MvpPresenter

/**
 * Created on : September/22/2017
 * Author     : Muhammad Fahmi Hidayah
 * Company    : PiXilApps
 * Project    : TestKotlin
 */
class MainPresenter : MvpBasePresenter<MainView>() {

    fun testMessage(nama : String) {
        view.show("Hello $nama")
    }

}