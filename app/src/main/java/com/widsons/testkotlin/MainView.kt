package com.widsons.testkotlin

import com.hannesdorfmann.mosby3.mvp.MvpView

/**
 * Created on : September/22/2017
 * Author     : Muhammad Fahmi Hidayah
 * Company    : PiXilApps
 * Project    : TestKotlin
 */
interface MainView : MvpView{
    fun show(message : String)
}