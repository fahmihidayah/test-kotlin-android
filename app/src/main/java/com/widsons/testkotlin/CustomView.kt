package com.widsons.testkotlin

import android.content.Context
import android.util.AttributeSet
import android.view.View

/**
 * Created on : September/22/2017
 * Author     : Muhammad Fahmi Hidayah
 * Company    : PiXilApps
 * Project    : TestKotlin
 */
class CustomView(context: Context?, attrs: AttributeSet?) : View(context, attrs) {

    fun setOnClick(listener : () -> Unit){
        setOnClickListener { listener() }
    }
}