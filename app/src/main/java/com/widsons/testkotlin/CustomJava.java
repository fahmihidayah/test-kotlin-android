package com.widsons.testkotlin;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created on : September/22/2017
 * Author     : Muhammad Fahmi Hidayah
 * Company    : PiXilApps
 * Project    : TestKotlin
 */
public class CustomJava extends View {
    public CustomJava(Context context) {
        super(context);
    }

    public CustomJava(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomJava(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    interface ListenerKu {
        void showTest();
    }

    public void setListenerKu(final ListenerKu lstKu){
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                lstKu.showTest();
            }
        });
    }
}
