package com.widsons.testkotlin

import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.hannesdorfmann.mosby3.mvp.MvpActivity
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : MvpActivity<MainView, MainPresenter>(), MainView{

    override fun createPresenter() = MainPresenter()


    override fun show(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        button_show
        var customView = findViewById(R.id.button_show)
        if(customView is CustomView){
            customView.setOnClick {
                presenter.testMessage("fahmi")
            }
        }
    }

    fun test(){
        object : AsyncTask<Void, Void?, Void>(){
            override fun doInBackground(vararg params: Void?): Void? {

                return null
            }

        }.execute()
    }
}
